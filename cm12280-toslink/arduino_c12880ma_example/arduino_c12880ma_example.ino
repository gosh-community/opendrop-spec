/*
 * Macro Definitions
 */
#define SPEC_TRG         A0
#define SPEC_ST          A1
#define SPEC_CLK         A2
#define SPEC_VIDEO       A3
#define WHITE_LED        A4
#define LASER_404        A5

#define SPEC_CHANNELS    288 // New Spec Channel
uint16_t data[SPEC_CHANNELS];

void setup(){

  //Set desired pins to OUTPUT
  pinMode(SPEC_CLK, OUTPUT);
  pinMode(SPEC_ST, OUTPUT);
  pinMode(LASER_404, OUTPUT);
  pinMode(WHITE_LED, OUTPUT);

  digitalWrite(SPEC_CLK, HIGH); // Set SPEC_CLK High
  digitalWrite(SPEC_ST, LOW); // Set SPEC_ST Low

  Serial.begin(230400); // Baud Rate set to 115200


  digitalWrite(LASER_404,1);
  digitalWrite(WHITE_LED,1);
}

/*
 * This functions reads spectrometer data from SPEC_VIDEO
 * Look at the Timing Chart in the Datasheet for more info
 */
void readSpectrometer(){


  int delayTime = 1; // delay time
#define WAIT() delayMicroseconds(delayTime)

  // Start clock cycle and set start pulse to signal start
  digitalWrite(SPEC_CLK, LOW);
  WAIT();
  digitalWrite(SPEC_CLK, HIGH);
  WAIT();
  digitalWrite(SPEC_CLK, LOW);
  // set start high, minimum length 381/f(CLK), integration starts at low to high
  // transition on the 4th clko pulse after ST low to high and ends
  // at the low to high transition of the 52 clock pulse after it goes low again. 
  // first video should be sampled on low to high on 89th clock after high to low on start
  digitalWrite(SPEC_ST, HIGH);
  WAIT();

  //Sample for a period of time
  for(int i = 0; i < 15+700; i++){

      digitalWrite(SPEC_CLK, HIGH);
      WAIT();
      digitalWrite(SPEC_CLK, LOW);
      WAIT(); 
 
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);

  //Sample for a period of time
  for(int i = 0; i < 85; i++){

      digitalWrite(SPEC_CLK, HIGH);
      WAIT();
      digitalWrite(SPEC_CLK, LOW);
      WAIT(); 
      
  }

  //One more clock pulse before the actual read
  digitalWrite(SPEC_CLK, HIGH);
  WAIT();
  digitalWrite(SPEC_CLK, LOW);
  WAIT();

  //Read from SPEC_VIDEO
  for(int i = 0; i < SPEC_CHANNELS; i++){

      data[i] = analogRead(SPEC_VIDEO);
      
      digitalWrite(SPEC_CLK, HIGH);
      WAIT();
      digitalWrite(SPEC_CLK, LOW);
      WAIT();
        
  }


  //Set SPEC_ST to high
  //digitalWrite(SPEC_ST, HIGH);

  //Sample for a small amount of time
  for(int i = 0; i < 7; i++){
    
      digitalWrite(SPEC_CLK, HIGH);
      WAIT();
      digitalWrite(SPEC_CLK, LOW);
      WAIT();
    
  }

  //digitalWrite(SPEC_CLK, HIGH);
  WAIT();
  
}

/*
 * The function below prints out data to the terminal or 
 * processing plot
 */
void printData(){
  
  for (int i = 0; i < SPEC_CHANNELS; i++){
    
    Serial.print(data[i]);
    Serial.print(',');
    
  }
  
  Serial.print("\n");
}

void loop(){
   
  readSpectrometer();
  printData();
  delay(10);  
   
}



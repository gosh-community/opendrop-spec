import os,sys,time,traceback
from FileLock import *


class Logger:
    def __init__(self,logfile,verbose=10,stdout=1,trace=0):
        self.logfile=logfile
        self.verbose=verbose
        self.stdout=stdout
        self.prepend_traceback=trace

    def LogStack(self,header="STACK",verbose=0):
        s=""
        stack=traceback.extract_stack(None)
        for filename,linenumber,method,text in stack[:-1]:
            dirname,filename=os.path.split(filename)
            dirname,modulename=os.path.split(dirname)
            filename,ext=os.path.splitext(filename)
            s+="%s.%s:%d %s\n"%(modulename,filename,linenumber,text)
        self.Log(header+"\n"+s,verbose)

    def LogException(self,header,verbose=0):
        """ Just returns a stack trace back as a string for logging exceptions """
        info=sys.exc_info()
        tb=traceback.format_exception(info[0],info[1],info[2],None)
        message=tb[-1]
        for line in tb[:-1]:
            message+=line
        self.Log(header+"\n"+message,verbose)

    def Log(self,message,verbose=0):
        if verbose>self.verbose: return
        self.BuildLogPath()
        """Write a message to the log with a timestamp and indentation.
        """
        ts="[%s]"%self.Time()
        if self.prepend_traceback:
            filename,linenumber,method,text=traceback.extract_stack(None,2)[0]
            dirname,filename=os.path.split(filename)
            dirname,modulename=os.path.split(dirname)
            filename,ext=os.path.splitext(filename)
            ts+="{%s.%s:%d %s} "%(modulename,filename,linenumber,method)
        ts+=" "
        
        indent="\n"+" "*len(ts)
        indented=indent.join(message.split("\n"))
        m="%s%s\n"%(ts,indented)
        try:
            if self.logfile==None or self.stdout:
                sys.stdout.write(m)
            if self.logfile:
                fd=OpenAndLock(self.logfile,"a")
                fd.write(m)
                CloseAndUnlock(fd)
        except:
            pass

    def BuildLogPath(self):
        pass
    
    def Time(self):
        """Formats current time for logging.
        """
        tm=time.localtime(time.time())
        ts="%04d-%02d-%02d %02d:%02d:%02d"%(tm[0],tm[1],tm[2],tm[3],tm[4],tm[5])
        return ts

    def Date(self):
        tm=time.localtime(time.time())
        date_str="%04d-%02d-%02d"%(tm[0],tm[1],tm[2])
        return date_str

class LoggerWithDatePath(Logger):
    def __init__(self,base_path,logname,verbose=0,stdout=1,trace=0):
        self.base_path=base_path
        self.logname=logname
        self.pathtime=0
        Logger.__init__(self,None,verbose,stdout,trace)
    def BuildLogPath(self):
        t=int(time.time())
        if self.pathtime==t: return
        self.pathtime=t
        self.logfile=os.path.join(self.base_path,self.Date(),self.logname)
            



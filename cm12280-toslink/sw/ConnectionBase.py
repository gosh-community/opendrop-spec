"""
Connection
- Trivial socket connection class 
(c) 2007 Bengt Sjolen, Stockholm, Sweden for EmergentBudapest

- Hacks by Istvan Rath, marked with FIXED
- Dav added ConnectionForever class that implements retry when connect times out or lost conn
- Dav added self.socket=None if the connect fails
- Bengt added non-blocking connect. If connect non-blocking you get a connection back 
- Note that there is no poll here since Connection is supposed to be
handled by a loop calling select on lots of connection and dispatching the handling of
it to the connections. Connection readable should result in a call to ReceiveFrame,
connection writable should result in a call to SendFrame and error in socket should
result in socket being killed.

SendFrame/ReceiveFrame are supposed to be called when there is space free in send buffer
and when there is data to receive

Peek gets current content of receive buffer without popping it
Read gets and pops from receive buffer
Write or Send appends the data to send queue

"""

import select,sys,os,time
from Logger import *
from errno import EALREADY, EINPROGRESS, EWOULDBLOCK, ECONNRESET, \
     ENOTCONN, ESHUTDOWN, EINTR, EISCONN, errorcode

ConnectionId=0

class ConnectionBase:
    def __init__(self,name="unknown",address="unknown",socket=None,logger=None,verbosity=10,connection_established_callback=None,connect_blocking=1):
        """Creates a Connection instance from a socket

        'name' is shortname of remote endpoint,
        'address' is BTADDR/MACADDR of remote endpoint,
        'socket' is the socket for the connection and
        'logger' is and instance of Logger class found in Logger.py
        """        
        self.name=name
        self.address=address
        self.socket=socket
        self.send_queue=""
        self.recv_queue=""
        self.bytes_recieved=0
        self.bytes_sent=0
        self.inpoll=0
        self.last_receive=time.time()
        self.last_send=time.time()

        """ this should be mutex-locked to be safe if we run several threads,
            current implementation of Server using select does however not thread! """
        global ConnectionId
        self.id=ConnectionId
        ConnectionId+=1

        self.logger=logger
        
        self.logname="%s(%s)"%(self.name,self.address)
        if self.logger: self.logger.Log("creating connection for %s"%self.logname,3)
        # setblocking on socket crashes if we send 1 or 0 instead of True or False

        self.kill=0

        self.err_names={ EALREADY:"EALREADY",
                         EINPROGRESS:"EINPROGRESS",
                         EWOULDBLOCK:"EWOULDBLOCK",
                         ECONNRESET:"ECONNRESET",
                         ENOTCONN:"ENOTCONN",
                         ESHUTDOWN:"ESHUTDOWN",
                         EINTR:"EINTR",
                         EISCONN:"EISCONN" }

        self.connecting=0
        self.connecting_ttl=3
        self.connecting_start=0
        self.connect_blocking=connect_blocking
        self.overflow=0
        self.connection_established_callback=connection_established_callback

    def SetOverflow(self,overflow=1):
        self.overflow=overflow
    def IsOverflowed(self):
        return self.overflow


    def fileno(self):
        """Get fileno of this connection.

        This makes it possible to use an instance of Connection with select since it
        needs the fileno of the underlying socket. Currently not used and should really
        return self.socket._sock.fileno() instead of self.socket.fileno() to work.
        """
        if self.socket==None:
            return -1
        return self.socket.fileno()

    def GetId(self):
        return self.id

    def GetAddress(self):
        """Get the BTADDR/MACADDR of the remote endpoint for this connection.
        """
        return self.address

    def IsConnecting(self):
        return self.connecting

    def DoConnect(self):
        pass

    def IsAlive(self):
        """Check if this connection is still present.

        Returns False for an already closed connection or True for a connection that is not
        closed.
        """
        return self.kill==0 and self.socket!=None

    def Kill(self):
        self.kill=1

    def Attach(self,socket):
        """Attach a socket to this Connection.

        'socket' is the socket to attach. The send queue is flushed.
        shouldn't the receive queue be flushed too?
        """
        self.socket=socket
        self.send_queue=""
        self.inpoll=0

    def Send(self,data):
        """Appends data to send buffer.
        """
        if self.IsAlive() and not self.IsConnecting():
            self.send_queue+=data

    def Close(self):
        """Close the socket.

        Closes the socket of this connection if it is connected.
        """
        if self.socket:
            if self.logger: self.logger.Log("closing connection %s"%self.logname,3)
            #self.logger.LogStack()
            self.socket.close()
            self.socket=None
        self.kill=0
        self.connecting=0
        self.send_queue=""
        self.recv_queue=""
        self.overflow=0

    def Fail(self,msg):
        """Log error message and close socket.
        """
        if self.logger: self.logger.Log("connection %s failed: %s"%(self.logname,msg),3)
        self.Close()
        return 0

    def IdleTime(self):
        """Get time since last receive or transmit on socket.
        """
        return time.time()-max(self.last_receive,self.last_send)

    def Touch(self):
        """ Update send time so that IdleTime becomes zero. If consumer of Connection
        wants to keep a connection from being flushed on when time-to-live seconds is exceeded it
        can touch it to keep it alive. """
        self.last_send=time.time()        

    def Inject(self,data):
        """Append data to receive buffer.
        """
        self.recv_queue+=data

    def Peek(self):
        """Returns contents of receive queue.

        This function should be used to peek into the receive buffer
        so that a user of this class can determine whether we have a full
        message that it's time to read.
        """
        return self.recv_queue

    def Flush(self,count=-1):
        """Flushes receive buffer
        Calling with count=-1 (default value) reads all of buffer
        """
        if count==-1: count=len(self.recv_queue)
        self.recv_queue=self.recv_queue[count:]
        
    def Read(self,count=-1):
        """Reads and removes data from receive queue.

        This method should be used to read data from a connection.
        Calling with count=-1 (default value) reads all of buffer
        """
        if count==-1: count=len(self.recv_queue)
        data=self.recv_queue[:count]
        self.recv_queue=self.recv_queue[count:]
        return data

    def ReadToDelimiter(self,delimiter='\0'):
        """Reads until delimiter is found and removes data incl the delimiter.
        Returns data excluding the delimiter.
        """
        cutpos=self.recv_queue.find(delimiter)
        if cutpos==-1:
            return ''
        else:
            ld=len(delimiter)
            return self.Read(cutpos+ld)[:-ld]        
    
    def Write(self,data):
        """Appends data to the send queue.

        This method should be used to send data over a connection.
        """
        #if self.logger: self.logger.Log("adding %d bytes of data to connection %s"%(len(data),self),4)
        if len(data):
            self.Send(data)

    def DoReceive(self):
        """INTERNAL: Read data from socket and append to recv_queue.

        This method should only called when select has told us that there is
        data to read. Otherwise we would block with a blocking socket. Returns
        number of bytes read which will be 0 if socket was closed.
        """        
        #if self.logger: self.logger.Log("data or event waiting from %s"%self.logname,5)
        try:
            data=self.OSRead(16384)
        except:
            data=""
        if len(data)==0:
            return self.Fail("length of received data is 0") # <-- FIXED, we must provide a parameter
        self.bytes_recieved+=len(data)
        self.last_receive=time.time()
        self.recv_queue+=data
        #if self.logger: self.logger.Log("got '%s'"%repr(data),20)
        return len(data)

    def OSSend(self,data): return self.socket.send(data)
    def OSRead(self,length): return self.socket.recv(length)

    def DoSend(self):
        """INTERNAL: Send data to socket removing from send_queue.

        This method should only be called when we know we can send because
        select told us so or we would block with a blocking socket and a full
        send buffer.
        """
        if len(self.send_queue)==0 or self.socket==None:
            self.overflow=0
            return 0
        try:
            sent=self.OSSend(self.send_queue)
        except:
            sent=0
        if sent==0:
            #print "sent==0: Connection.DoSend %s %s sent %d"%(self,repr(self.send_queue),sent)
            return -1
        #else:
        #    print "sent!=0: Connection.DoSend %s %s sent %d"%(self,repr(self.send_queue),sent)            
        self.bytes_sent+=sent
        self.last_send=time.time()
        self.send_queue=self.send_queue[sent:]
        if len(self.send_queue)==0:
            self.overflow=0
        else:
            self.logger.Log("blocking on %d bytes in sendqueue"%(len(self.send_queue)))
                            
        
        return sent

    def Debug(self,data,offset=-1):
        """Hexdump data for debugging.
        """
        lines=[]
        if offset==-1: offset=self.bytes_recieved
        shex,schr="%04x "%offset,""
        for i in range(0,len(data)):
            c=ord(data[i])
            shex+="%02x "%c
            if c>=32 and c<128: schr+="%c"%data[i]
            else: schr+="."
            if (i&15)==15:
                lines+=["%-50s %-16s"%(shex,schr)]
                shex,schr="%04x "%(offset+i+1),""
        if len(schr)>0:
            lines+=["%-55s %-16s"%(shex,schr)]
        return "\n".join(lines)

    def Socket(self):
        """Get the socket for this connection.
        """
        return self.socket

    def OSSocket(self):
        """Get the real socket for this connection.
        """
        if self.socket: return self.socket._sock

    def SendFrame(self):
        """Try to send pending data.
        """
        tx=self.DoSend()
        if tx<0:
            if self.logger: self.logger.Log("%s was broken on send"%self.logname,8)
            self.Close()
            return -1
        return tx

    def ReceiveFrame(self):
        """Try to receive.
        """
        rx=self.DoReceive()
        if rx==0:
            if self.logger: self.logger.Log("%s was disconnected"%self.logname,8)
            self.Close()
            return -1        
        return rx

    def TryReconnect(self):
        return 0

    def Poll(self,dt=0.01,receiver=None):
        if not self.socket:
            if not self.TryReconnect():
                return 0
        if self.connecting:
            if not self.DoConnect():
                return 0

        self.SetOverflow(1)
            
        sockets=[self.OSSocket()] #[self.socket._sock]
        re,we,err=select.select(sockets,sockets,sockets,dt)
        #print "select: ",re,we,err
        rxsum,txsum=0,0
        for rsocket in re:
            rx=self.ReceiveFrame()
            rxsum+=rx
        
            if rx<0:
                self.Kill()
                if self.logger: self.logger.Log("connection from %s was closed by remote end"%(self.logname),2)
                rx=-rx
            elif receiver:
                receiver(self)
            #elif rx>0:
            #    d=len(self.Peek())
                
                    
        for wsocket in we:
            tx=self.SendFrame()
            if tx<0:
                self.Kill()
                if self.logger: self.logger.Log("failure during SendFrame so we schedule connection for killing",2)
                tx=-tx
            txsum+=tx
            
        for esocket in err:
            if self.logger: self.logger.Log("select returns error for connection %s"%self.logname,2)
            self.Kill()

        if self.kill: self.Close()
        
        return rxsum+txsum

    def Process(self,t):
        return 0
    
    def Run(self,dt=0.0,debug=0):
        """Call Poll and Process in Server forever.
        """
        start=time.time()
        while dt==0.0 or time.time()<start+dt:
            d=0
            t=time.time()
            d+=self.Poll()
            d+=self.Process(t)
            if debug:
                data=self.Read()
                if data:
                    self.logger.Log(self.Debug(data))
            if d==0:
                self.logger.Log("Connection.Run sleeping",20)
                time.sleep(0.0001)

        
        

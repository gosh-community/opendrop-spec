import select,sys,os,time
import SerialTO
from Logger import *
from ConnectionBase import *

class ConnectionSerial(ConnectionBase):
    def __init__(self,name="unknown",address="unknown",tty_list=None,logger=None,verbosity=10,connection_established_callback=None,connect_blocking=1,speed=115200,rtscts=False):
        ConnectionBase.__init__(self,name,address,None,logger,verbosity,connection_established_callback,connect_blocking)

        self.tty_list=tty_list
        self.speed=speed
        self.socket=None
        self.last_connect=0
        self.reconnect_interval=2.0
        self.rtscts=rtscts

        self.TryReconnect()

    def Connect(self,tty_list,speed):
        self.tty_list=tty_list
        self.speed=speed
        self.DoConnect()

    def TryReconnect(self):
        if self.socket: return
        if self.last_connect+self.reconnect_interval<time.time():
            self.last_connect=time.time()
            return self.DoConnect()

    def DoConnect(self):
        tty=None
        if type(self.tty_list)==int:
            ports=self.FindSerialPorts()
            if self.tty_list<len(ports):
                tty=ports[self.tty_list]
        elif type(self.tty_list)==list:
            ports=self.FindSerialPorts(self.tty_list)
            if len(ports):
                tty=ports[0]
        elif type(self.tty_list)==str:
            tty=self.tty_list
        else:
            self.socket=self.tty_list
            self.last_connect=time.time()

        if tty:
            self.tty=tty
            self.logger.Log("Connecting to TTY: "+tty)
            try:
                self.socket=SerialTO.SerialTO(tty,self.speed,timeout=0,rtscts=self.rtscts)
                self.socket.setTimeout(0)
                self.last_connect=time.time()
            except:
                self.logger.Log("exception opening port at: "+tty)
                return 0
        if self.socket:
            if self.connection_established_callback:
                self.connection_established_callback(self)
            return 1
            
    def FindSerialPorts(self,devices=None):
        #return ["tty.usbserial"]
        if not devices:
            devices=["tty.usbserial","tty.PL2303","tty.usbmodem"]
            #devices+=["ttyUSB"]
        dev="/dev/"
        devices=map(lambda x: x.replace(dev,""),devices)
        files=os.listdir(dev)
        #if f!=None: f.write("   /dev/: %s\n"%str(files))
        ports=[]
        for name in files:
            for device in devices:
                #if f!=None: f.write("   %s == %s = %s\n"%(name[:len(path)],path,name[:len(path)]==path))
                if name[:len(device)]==device:
                    ports.append(dev+name)
        return ports

    def OSSocket(self):
        """Get the real socket for this connection.
        """
        if self.socket: return self.socket.fd
    def OSSend(self,data): return self.socket.write(data)
    def OSRead(self,length): return self.socket.read(length)

    def Process(self,t):
        if 1:
            data=self.ReadToDelimiter(delimiter="\n")
            #data=self.Read()
            if data:
                #self.logger.Log(self.Debug(data))
                self.logger.Log(data)
            
        return 0


def main(argv,stdout,environ):
  progname = argv[0]
  args=argv[1:]
  
  logger=Logger(None,10)

  tty_list=["/dev/ttyACM1"]
  c=ConnectionSerial(logger=logger,tty_list=tty_list,speed=115200)
  c.Run()

if __name__ == "__main__":
  main(sys.argv, sys.stdout, os.environ)

import sys,os

if sys.platform!='win32':
	import fcntl
	def OpenAndLock(path,mode="r+"):
		# It is an error condition of course if the path points to a directory
		# but that will raise an exception later anyway
		dirpath=os.path.dirname(path)
		if not os.path.isdir(dirpath):
			os.makedirs(dirpath)
		if not os.path.isdir(dirpath):
			raise "OpenAndLock: failed to create directory '%s'"%dirpath

		# os.open throws an exception on fail, create file if it does not exist
		# note that we are still open for race-condition on the first create as this
		# one creates the file and another process could lock the file before
		# we get to write, but that is just on the first creation...
		old_mask=os.umask(0)
		fd=os.open(path,os.O_RDWR+os.O_CREAT,0660)
		if fd<0:
			os.umask(old_mask)
			raise "OpenAndLock: bad filedescriptor received (%d)"%fd
		# open for reading and writing with stream position at the beginning
		fd_file=os.fdopen(fd,mode)
		os.umask(old_mask)
		# obtain an advisory exclusive lock, might raise an exception like
		# keyboard interrupt
		failed=0
		while 1:
			try:
				# wow lockf is definately not working...
				fcntl.flock(fd_file,fcntl.LOCK_EX)
				return fd_file
			except KeyboardInterrupt:
				pass
			except:                
				raise "OpenAndLock: failed obtaining exclusive lock for '%s'"%path

	def CloseAndUnlock(fd_file):
		fd_file.close()
else:
	def OpenAndLock(path,mode="r+"):
		# It is an error condition of course if the path points to a directory
		# but that will raise an exception later anyway
		dirpath=os.path.dirname(path)
		if not os.path.isdir(dirpath):
			os.makedirs(dirpath)
		if not os.path.isdir(dirpath):
			raise "OpenAndLock: failed to create directory '%s'"%dirpath

		# os.open throws an exception on fail, create file if it does not exist
		# note that we are still open for race-condition on the first create as this
		# one creates the file and another process could lock the file before
		# we get to write, but that is just on the first creation...
		old_mask=os.umask(0)
		fd=os.open(path,os.O_RDWR+os.O_CREAT,0660)
		if fd<0:
			os.umask(old_mask)
			raise "OpenAndLock: bad filedescriptor received (%d)"%fd
		# open for reading and writing with stream position at the beginning
		fd_file=os.fdopen(fd,mode)
		os.umask(old_mask)
		# obtain an advisory exclusive lock, might raise an exception like
		# keyboard interrupt
		failed=0
		while 1:
			try:
				# wow lockf is definately not working...
				#fcntl.flock(fd_file,fcntl.LOCK_EX)
				return fd_file
			except KeyboardInterrupt:
				pass
			except:                
				raise "OpenAndLock: failed obtaining exclusive lock for '%s'"%path

	def CloseAndUnlock(fd_file):
		fd_file.close()




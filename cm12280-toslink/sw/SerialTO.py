from serial import *

class SerialTO(Serial):
    def write(self, data):
        """Output the given string over the serial port."""
        if self.fd is None: raise portNotOpenError
        return os.write(self.fd,data)

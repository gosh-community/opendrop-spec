"""

PA Plot absolute
PR Plot relative

We need a timeout during which to collect coordinates, any transition
between up and down or pen selects MUST be output - to be correct
any coordinate along a vector can be approximated to that vector and
this might actually be better?

PA x,y; might be 14 characters
at 9600 we have 960 charactes per second and
one PA per frame should be 60*14 = 840 characters/second so just within
margins - however the plotter might not be as happy about small moves and 
stopping...







"""


import sys,serial,select,time,os,random,struct,thread,threading
VisiblePath=os.path.abspath("../../../Visible")
ShellPath=os.path.join(VisiblePath,"Shell-1")
if not ShellPath in sys.path: sys.path=[ShellPath,VisiblePath,".."]+sys.path

if 0:
    AutomataPath=os.path.abspath("../../.."+"/Automata")
    if not AutomataPath in sys.path: sys.path=[AutomataPath]+sys.path
    #AutomataPath=os.path.abspath("../.."+"/Automata")
    #if not AutomataPath in sys.path: sys.path=[AutomataPath]+sys.path


import math,Queue

from MidiApplication import *
from MidiScene import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from Texture import *
#from Envelope import *
from FontBitmap import *
from FontFT2 import *
from ImagePool import *
from ConnectionSerial import ConnectionSerial
from ConnectionFile import ConnectionFile
from Math import V2,L2,M2,B2,PPolygon,PVertex,M3
from Reloader import *
import numpy as np


class TestScene(MidiScene):
    def __init__(self,name,application):
        MidiScene.__init__(self,name,application,application.logger)
        self.AddPixelCamera("pixel")
        self.SelectCamera("pixel")
        
        self.AddCamera("perspective")
        cam=self.GetCamera("perspective")
        cam.SetPos((0,150,0))
        cam.SetLookAt((0,0,0))
        cam.SetUp(0,0,1)
        cam.SetNearFar(1,1000)

        self.AddSpecial(range(32,10240),self.Special)
        self.AddKeypress("".join(map(lambda x: chr(x),range(1,256))),self.Key)

        bitmapfont=FontBitmap("2513.bin","2513.bin",{})
        TheFontPool.Add(bitmapfont,"2513.bin",{})
        
        self.logger=Logger(None,10)
        speed=115200
        speed=230400
        self.c=ConnectionSerial(logger=self.logger,tty_list=["/dev/ttyACM1","/dev/ttyACM0"],speed=speed)
        #self.c=ConnectionFile(logger=self.logger,path="../../Application/4col1-2014-01-17")


        

        self.cursor=0

        self.appstart=time.time()

        self.t_keypress=0

        self.reloader=Reloader("config.py",interval=0.5)


    def Load(self,t):
        MidiScene.Load(self,t)

        self.history=[]

        self.history_length=10

        self.data=np.array([0.0]*288,dtype=np.float64)
        self.summed=np.array([0.0]*288,dtype=np.float64)

        self.PushHistory(self.data)
        self.SetBlank()

        # i have 17A00658
        # calibration for 17A00672
        self.calibration=3.155203937e2,2.691366611,-1.204986274e-3,-6.968476968e-6,6.308939537e-9,9.448126009e-12

        wavelengths=[]
        for i in range(0,288):
            wavelengths+=[self.PixelToWavelength(i)]
        self.wavelengths=wavelengths

        colors=[]
        colors1=[]
        for w in wavelengths:
            colors+=[self.wavelength_to_rgb(w,gamma=0.8)]
            colors1+=[self.wavelength_to_rgb(w,gamma=0.8,black=0)]
        self.colors=colors
        self.colors1=colors1
            

        self.show_data=0
        self.show_blank=0
        self.show_average=0
        self.has_blank=0

    def PixelToWavelength(self,pix):
        A0,B1,B2,B3,B4,B5=self.calibration
        pow=math.pow
        return A0+B1*pix+B2*pow(pix,2)+B3*pow(pix,3)+B4*pow(pix,4)+B5*pow(pix,5)
        

    def PushHistory(self,data):
        self.history+=[data]
        self.summed+=data
        while len(self.history)>self.history_length:
            old=self.history.pop(0)
            self.summed-=old


    def GetAverage(self):
        return self.summed/len(self.history)

    def SetBlank(self):
        self.blank=self.GetAverage()

    def ZeroBlank(self):
        self.blank=np.array([0.0]*288,dtype=np.float64)



    def LogException(self,header="EXCEPTION",verbose=0):
        """ Just returns a stack trace back as a string for logging exceptions """
        info=sys.exc_info()
        tb=traceback.format_exception(info[0],info[1],info[2],None)
        message=tb[-1]
        for line in tb[:-1]:
            message+=line
        e=header+"\n"+message
        print e
        

    def Special(self,key,x,y):
        self.t_keypress=time.time()
        if key==GLUT_KEY_UP:
            print "cursor =",self.cursor
        elif key==GLUT_KEY_DOWN:
            print "cursor =",self.cursor

        
        

    def Key(self,key,x,y):
        self.t_keypress=time.time()
        print "Key",repr(key)

        if key=="b":
            self.SetBlank()
            self.has_blank=1
        if key=="0":
            self.ZeroBlank()
            self.has_blank=0
        if key=="a":
            self.show_average=1-self.show_average
        if key=="d":
            self.show_data=1-self.show_data
        if key=="f":
            self.show_blank=1-self.show_blank

        t_now=time.time() # why is this not fed from app/scene?!?
        kv=ord(key)



    def OnMouse(self,button,state,x,y):
        #print "OnMouse",x,y,button,state
        # should be GLUT_LEFT_BUTTON?
        pass

    def OnMotion(self,x,y):
        #print "OnMotion",x,y
        self.MousePosition=(x,y)
    
    def OnPassiveMotion(self,x,y):
        #print "OnPassiveMotion",x,y
        self.MousePosition=(x,y)

        
    def OnExit(self):
        print "scene.OnExit!"

    def Update(self,t_now,duration,fader=1.0):
        MidiScene.Update(self,t_now,duration,fader)
        self.reloader.LoadOnChange()
        d=self.c.Poll()
        retries=0
        verbose=0


        while 1:
            rawdata=self.c.ReadToDelimiter(delimiter="\n")
            if not rawdata: break
            if rawdata:
                if verbose: print repr(rawdata)
                
                if 1:
                    parts=rawdata.split(",")[:-1]
                    if len(parts)==288:

                        data=np.array(map(lambda x:int(x),parts),dtype=np.float64)

                        self.PushHistory(data)                    
                        self.data=data

            
        

    def Heatmap(self,t):
        t=max(0.0,min(1.0,t))
        # blue is 0.5
        c1=t*8.0
        c=int(c1)
        m=0.5*(c1%1.0)
        r,g,b=0,0,0
        if c==0:
            b=0.5+m
        elif c==1:
            b,g=1.0,m
        elif c==2:
            b,g=1.0,0.5+m
        elif c==3:
            b,g,r=1.0-m,1.0,m
        elif c==4:
            b,g,r=0.5-m,1.0,0.5+m
        elif c==5:
            g,r=1.0-m,1.0
        elif c==6:
            g,r=0.5-m,1.0
        elif c==7:
            r=1.0-m
        return r,g,b

    def wavelength_to_rgb(self,wavelength, gamma=0.8,black=1):

        '''This converts a given wavelength of light to an 
        approximate RGB color value. The wavelength must be given
        in nanometers in the range from 380 nm through 750 nm
        (789 THz through 400 THz).
        
        Based on code by Dan Bruton
        http://www.physics.sfasu.edu/astro/color/spectra.html
        '''

        wavelength = float(wavelength)
        if wavelength >= 380 and wavelength <= 440:
            attenuation = 0.3 + 0.7 * (wavelength - 380) / (440 - 380)
            R = ((-(wavelength - 440) / (440 - 380)) * attenuation) ** gamma
            G = 0.0
            B = (1.0 * attenuation) ** gamma
        elif wavelength >= 440 and wavelength <= 490:
            R = 0.0
            G = ((wavelength - 440) / (490 - 440)) ** gamma
            B = 1.0
        elif wavelength >= 490 and wavelength <= 510:
            R = 0.0
            G = 1.0
            B = (-(wavelength - 510) / (510 - 490)) ** gamma
        elif wavelength >= 510 and wavelength <= 580:
            R = ((wavelength - 510) / (580 - 510)) ** gamma
            G = 1.0
            B = 0.0
        elif wavelength >= 580 and wavelength <= 645:
            R = 1.0
            G = (-(wavelength - 645) / (645 - 580)) ** gamma
            B = 0.0
        elif wavelength >= 645 and wavelength <= 750:
            attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645)
            R = (1.0 * attenuation) ** gamma
            G = 0.0
            B = 0.0
        else:
            if black:
                R,G,B=0.0,0.0,0.0
            else:
                R,G,B=1.0,0.0,1.0
        return R,G,B
            
            


    def Render(self,t_now,duration,fader=1.0,data=None):
        start=time.time()
        self.GetCamera("pixel").Project()

        if 0 and self.cursor==0:
            self.background=1.0,0.0,0.0,0.0
        else:
            self.background=1.0,1.0,1.0,0.0
            self.background=0.0,0.0,0.0,0.0

        glClearColor(*self.background)

        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)
        glEnable(GL_MULTISAMPLE)
        glEnable(GL_POLYGON_SMOOTH)


        summed=self.summed
        data=self.data
        summed_max=max(summed)
        
        sw,sh=self.application.GetGeometry()
        x0,y0=100,sh-200

        y0+=150
        y1=y0-875

        yscale=0.7

        def RenderCurve(x0,y0,color,data):
            glLineWidth(2.0)
            if type(color)==list:
                glBegin(GL_LINE_STRIP)            
                for i in range(0,len(data)):
                    if i<len(color):
                        glColor3f(*color[i])
                    glVertex3f(x0+i*2,y0-(875-data[i]*yscale),0)
                glEnd()
            else:
                glColor3f(*color)
                glBegin(GL_LINE_STRIP)            
                for i in range(0,len(data)):
                    glVertex3f(x0+i*2,y0-(875-data[i]*yscale),0)
                glEnd()
            glLineWidth(1.0)

        def RenderCurveColored(x0,y0,color,data):
            #RenderCurve(x0,y0,color,data)
            RenderCurve(x0,y0-1,self.colors1,data)


        g=0.3
        average=self.GetAverage()
        if self.show_blank:
            RenderCurve(x0,y0,(0,0,g),self.blank)

        selected=None
        if self.show_data or not self.has_blank:
            print "data",self.show_data,self.has_blank
            if self.show_average:
                selected=average
                RenderCurveColored(x0,y0,(g,g,g),selected)
            else:
                selected=data
                RenderCurveColored(x0,y0,(g,g,g),selected)
        else:        
            absorption=self.blank-self.data
            absorption_average=self.blank-average

            # 111.5 1023.0 -108.1 892.7 111.2 1023.0
            #print min(average),max(average),min(absorption_average),max(absorption_average),min(self.blank),max(self.blank)
            
            if self.show_average:
                selected=absorption_average
                RenderCurveColored(x0,y0,(g,g,g),selected)
            else:
                selected=absorption
                RenderCurveColored(x0,y0,(g,g,g),selected)


        
        glColor3f(1,1,1)

        glBegin(GL_LINES)
        glVertex3f(x0,y1-1,0)
        glVertex3f(x0+288*2,y1-1,0)

        glVertex3f(x0,y1-1,0)
        glVertex3f(x0,y0,0)
        glEnd()

        w=self.wavelengths
        # render spectrum
        
        glBegin(GL_QUAD_STRIP)
        for i in range(0,len(w)):
            glColor3f(*self.colors[i])
            x1=x0+i*2
            glVertex3f(x1,y1-22,0)
            glVertex3f(x1,y1-42,0)
        glEnd()
            
        glColor3f(1,1,1)
        
        # render wavelengths
        div=10.0
        glBegin(GL_LINES)
        ticks=[]
        for i in range(1,len(w)):
            w0=w[i-1]            
            w1=w[i+0]
            
            d0=int(w0/div)
            d1=int(w1/div)
            if d0!=d1:
                #print w1,w1-w0
                
                x1=x0+i*2

                wavelength=d1*div
                d=5
                if (wavelength%50)==0:
                    d=10
                    ticks+=[(wavelength,x1,y1)]

                glVertex3f(x1,y1-1,0)
                glVertex3f(x1,y1-1-d,0)
        yticks=[]
        div=20.0
        for i in range(0,1024):
            w0=i-1
            w1=i
            d0=int(w0/div)
            d1=int(w1/div)
            if d0!=d1:
                y2=y0-(875-i*yscale)            
                d=5
                if (i%100)==0:
                    d=10
                    yticks+=[(i,x0,y2)]
                glVertex3f(x0-d,y2,0)
                glVertex3f(x0-0,y2,0)
                
             
        glEnd()

        for d1,x1,y1 in ticks:
            color=1,1,1
            fsize=8
            s=1.0/64*fsize
            s=s,s,s
            tstr="%d"%d1
            self.RenderText("2513.bin",(x1-fsize*1.5+1,y1-20,0),64,tstr,alpha=1.0,rgb=color,rot=0.0,scale=s)

        for d1,x2,y2 in yticks:
            color=1,1,1
            fsize=8
            s=1.0/64*fsize
            s=s,s,s
            tstr="%4d"%d1
            self.RenderText("2513.bin",(x2-fsize*4.0-4,y2-4,0),64,tstr,alpha=1.0,rgb=color,rot=0.0,scale=s)

            


        pos=self.MousePosition
        if pos:
            x2,y2=pos
            y2=sh-y2

            d=(x2-x0)/2
            if d>0 and d<len(self.wavelengths):        
                color=1,1,1
                fsize=8
                s=1.0/64*fsize
                s=s,s,s
                m=max(selected)
                A=selected[d]
                if m: A/=m
                w=self.wavelengths[int(d)]
                tstr="%5.1f A=%5.3f"%(w,A)
                self.RenderText("2513.bin",(x2-fsize*5.0+1,y2+5,0),64,tstr,alpha=1.0,rgb=color,rot=0.0,scale=s)
        
                g=0.5
                glColor3f(g,g,g)
                glBegin(GL_LINES)
                glVertex3f(x2,y1-1,0)
                glVertex3f(x2,y0,0)
                glEnd()

        # peak detect

        if 1:
            w=10
            threshold=5.0
            for i in range(w,len(selected)-w):
                mx=selected[i]
                
                ismax=1
                for j in range(i-w,i+w):
                    if j!=i and selected[j]>mx: 
                        ismax=0
                        break
                if ismax:
                    m1=mx-selected[i-w+0]
                    m2=mx-selected[i+w-1]
                    if not (m1>threshold and m2>threshold):
                        ismax=0
                
                if ismax:
                    x2=x0+i*2
                    y2=int(y0-(875-mx*yscale))
                    
                    glColor3f(1,1,1)
                    glBegin(GL_LINE_LOOP)
                    r=5-0
                    for k in range(0,32):
                        a=math.pi/16*k
                        sa,ca=math.sin(a),math.cos(a)
                        glVertex3f(x2+ca*r,y2+sa*r,0)
                    glEnd()

                    color=1,1,1
                    fsize=8
                    s=1.0/64*fsize
                    s=s,s,s
                    m=max(selected)
                    A=selected[i]
                    if m: A/=m
                    wl=self.wavelengths[int(i)]
                    tstr="%5.1f A=%5.3f"%(wl,A)
                    self.RenderText("2513.bin",(x2-fsize*5.0+1,y2+10,0),64,tstr,alpha=1.0,rgb=color,rot=0.0,scale=s)
                    
                        
                        
                
                    
                



            
class TestApplication(MidiApplication):
    def __init__(self,w,h,fullscreen):
        MidiApplication.__init__(self,w,h,"Plotter",fullscreen=0,multisample=8)
        self.scene_index=-1

    def Frame(self,now):
        MidiApplication.Frame(self,now)

    def NextScene(self):
        self.scene_index=(self.scene_index+1)%len(self.sequence)
        scene,duration=self.sequence[self.scene_index]
        print "switching to %s"%scene
        self.Scenes.FadeTo(scene,0.5,0.5,0.0)
        self.next_change=time.time()+duration

    def LoadFirstPresets(self):
        for scene in self.Scenes.sequence:
            scene.NextPreset()



def main(argv,stdout,environ):
  progname = argv[0]
  args=argv[1:]

  w,h=1024,1024
  app=TestApplication(w,h,0)

  scenes=app.GetScenes()

  test1=TestScene("test1",app)
  scenes.Add(test1)  
  # Load before activate!
  app.Load()
  #app.LoadFirstPresets()
  scenes.Set("test1")
  
  #app.NextScene()
  app.Run()



if __name__ == "__main__":
  main(sys.argv, sys.stdout, os.environ)

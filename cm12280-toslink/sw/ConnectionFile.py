import select,sys,os,time
import SerialTO
from Logger import *
from ConnectionBase import *

class ConnectionFile(ConnectionBase):
    def __init__(self,name="unknown",address="unknown",path=None,logger=None,verbosity=10,connection_established_callback=None,connect_blocking=1):
        ConnectionBase.__init__(self,name,address,None,logger,verbosity,connection_established_callback,connect_blocking)

        self.path=path
        self.pos=0

        self.socket=None
        self.last_connect=0
        self.reconnect_interval=2.0

        self.TryReconnect()

    def Connect(self,tty_list,speed):
        self.tty_list=tty_list
        self.speed=speed
        self.DoConnect()

    def TryReconnect(self):
        #print "TryReconnect"
        #print "try reconnect",self.socket
        if self.socket: return
        #if self.socket: print "self.socket",self.socket,"is True"
        #else:           print "self.socket",self.socket,"is False"
        #print self.last_connect,self.reconnect_interval,self.last_connect+self.reconnect_interval-time.time()
        if self.last_connect+self.reconnect_interval<time.time():
            self.last_connect=time.time()
            return self.DoConnect()

    def DoConnect(self):
        print "DoConnect"
        if not os.path.isfile(self.path):
            print "no such file",self.path
            return
        self.socket=file(self.path)
        print self.path+":",self.socket
        self.last_connect=time.time()
        if self.socket:
            if self.connection_established_callback:
                self.connection_established_callback(self)
            return 1
    def OSSocket(self):
        """Get the real socket for this connection.
        """
        return self.socket
    def OSSend(self,data):
        print "ignoring write:\n"+self.Debug(data)
        return len(data)
    def OSRead(self,length):
        data=self.socket.read(length)
        if len(data)<length:
            self.socket.seek(0)
            data+=self.socket.read(length-len(data))
        return data


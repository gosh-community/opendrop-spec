import os,sys,string,time,math,random

home=os.getenv("HOME")
ShellPath=os.path.join(home,"Code/Visible/Shell-1")
VisiblePath=os.path.join(home,"Code/Visible")
CAMPath=os.path.join(home,"Code/CAM2")
PCBPath=os.path.join(home,"Code/PCB")
if not ShellPath in sys.path: sys.path=[ShellPath,VisiblePath,CAMPath,PCBPath]+sys.path

from Polygon import*
from NCEncoder import *
from SVGEncoder import *
from MillBase import *
from Math import *
from PCBManipulator import *

import Viewer
from Centroid import *

class Miller(MillBaseDepth):
    def __init__(self,offset=None):
        MillBaseDepth.__init__(self,offset)
        self.zabove_safe=45.0
        self.millingdistance=0.15/2
        self.action=None

    def build_tubestand(self,args):
        ################################################################################
        # tube stand
        ################################################################################
        zthrough=-1.0
        self.millingdistance=0.0 #0.15/2

        material1=4.0
        ny=3
        nx=6

        x0,y0=0,0
        w,h=200.0,40.0
        w1,h1=50.0,h

        ################################################################################
        # tube measurements
        ################################################################################
        epp10d=10.7
        epp10h=37.0
        epp10df=6.6
        # df is diameter 10mm below? excluding 3mm material thickness

        epp02d=7.8
        epp02h=27.0
        epp02df=5.6


        if args[0]=="10":
            tubd=epp10d
            tubh=epp10h
            tubh=20.0

            tubdf=epp10df
            stagger=1
            floor=1
            floorholes=1
            floordist=9.0
            floorh=tubh-floordist-material1
            cdistance=8.0
            cdistance1=8.0
            hmargin=3.0
        elif args[0]=="2":
            tubd=epp02d
            tubh=epp02h
            tubh=20.0

            tubdf=epp02df
            stagger=1
            floor=1
            floorholes=1
            floordist=9.0
            floorh=tubh-floordist-material1
            cdistance=8.0
            cdistance1=8.0
            hmargin=3.0
        
        cdistance2=cdistance1+material1

        x1,y1=x0,y0

        taph=8.0
        tapw=taph
        d=material1
        rcw=cdistance2+tubd*nx+cdistance*(nx-1)+cdistance2
        rch=cdistance1+tubd*ny+cdistance*(ny-1)+cdistance1
        if floor:
            if floorholes:
                rchh=tubh+material1+5.0
            else:
                rchh=tubh+material1+5.0
        else:
            rchh=tubh
        rchh+=hmargin

        x1,y1=x0,y0


        ctapsides={
                #"left":(1,[rch*0.25,rch*0.75]),
                "right":(1,[rch*0.25,rch*0.75]),
                #"left":(-1,[rch/2+th2],material1*2),
                #"right":(-1,[rch/2+th2],material1*2),
        }        

        ctapsides1={
                "left":(-1,[rch*0.5]),
                "right":(-1,[rch*0.5]),
        }

        ctop={
            #"top":(-1,[rcw/2+tw2]),
            #"bottom":(-1,[rcw/2+tw2]),
            "left":(-1,[rch*0.25,rch*0.75],material1*1),
            "right":(-1,[rch*0.25,rch*0.75],material1*1),
        }        

        cbottom={
            #"top":(-1,[rcw/2+tw2]),
            #"bottom":(-1,[rcw/2+tw2]),
            "left":(1,[rch*0.25,rch*0.75],material1*1),
            "right":(1,[rch*0.25,rch*0.75],material1*1),
        }        


        # top
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctop,tapd=material1,tapw=taph)
        for ny1 in range(0,ny):
            oox=0
            nx0=nx
            if stagger and ny1&1:
                oox=(cdistance+tubd)*0.5
                nx0-=1
                
            for nx1 in range(0,nx0):
                q.AddCircle(zthrough,
                               x1+cdistance2+(cdistance+tubd)*nx1+tubd*0.5+oox,
                               y1+cdistance1+(cdistance+tubd)*ny1+tubd*0.5,
                               tubd,nverts=64)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0



        # empty sides
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rchh,rch,taps=ctapsides,tapd=material1,tapw=taph)
        if floor:
            q.AddRectangle(zthrough,x1+floorh,y1+rch*0.25-taph*0.5,material1,taph,centered=0)
            q.AddRectangle(zthrough,x1+floorh,y1+rch*0.75-taph*0.5,material1,taph,centered=0)

        # reference 20x20 mm
        if 0:
            ref=20.0
            q.AddRectangle(zthrough,x1,y1,ref,ref,centered=0)
            ref=40.0
            q.AddRectangle(zthrough,x1,y1,ref,ref,centered=0)
        self.Place(V3(0,0),(p,q),0)


        if floor:
            y1+=rch+5.0
        else:
            x1+=rchh+5.0


        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rchh,rch,taps=ctapsides,tapd=material1,tapw=taph)
        if floor:
            q.AddRectangle(zthrough,x1+floorh,y1+rch*0.25-taph*0.5,material1,taph,centered=0)
            q.AddRectangle(zthrough,x1+floorh,y1+rch*0.75-taph*0.5,material1,taph,centered=0)
        self.Place(V3(0,0),(p,q),0)

        x1+=rchh+5.0
        

        x1,y1=x0,y0+rch+5.0


        # bottom
        if floor:
            p=PolygonSet()
            q=PolygonSet()
            p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=cbottom,tapd=material1,tapw=taph)
            
            if floorholes:
                for ny1 in range(0,ny):
                    oox=0
                    nx0=nx
                    if stagger and ny1&1:
                        oox=(cdistance+tubd)*0.5
                        nx0-=1

                    for nx1 in range(0,nx0):
                        q.AddCircle(zthrough,
                                       x1+cdistance2+(cdistance+tubd)*nx1+tubd*0.5+oox,
                                       y1+cdistance1+(cdistance+tubd)*ny1+tubd*0.5,
                                       tubdf,nverts=64)


            self.Place(V3(0,0),(p,q),0)


    def build_cuvettestand(self,args):
        ################################################################################
        # cuvette stand
        ################################################################################
        zthrough=-1.0
        self.millingdistance=0.0 #0.15/2

        material1=4.0
        ny=2
        nx=6


        x0,y0=0,0
        w,h=200.0,40.0
        w1,h1=50.0,h

        ################################################################################
        # standard cuvette measurements
        ################################################################################

        cuvw=12.5
        cuvh=12.5
        cuvhh=45.0

        
        cdistance=8.0
        cdistance1=6.0
        cdistance2=cdistance1+material1

        x1,y1=x0,y0

        taph=8.0
        tapw=taph
        d=material1
        rcw=cdistance2+cuvw*nx+cdistance*(nx-1)+cdistance2
        rch=cdistance1+cuvh*ny+cdistance*(ny-1)+cdistance1
        rchh=cuvhh*0.5+material1

        x1,y1=x0,y0


        ctapsides={
                #"left":(1,[rch*0.25,rch*0.75]),
                "right":(1,[rch*0.25,rch*0.75]),
                #"left":(-1,[rch/2+th2],material1*2),
                #"right":(-1,[rch/2+th2],material1*2),
        }        

        ctapsides1={
                "left":(-1,[rch*0.5]),
                "right":(-1,[rch*0.5]),
        }

        ctop={
            #"top":(-1,[rcw/2+tw2]),
            #"bottom":(-1,[rcw/2+tw2]),
            "left":(-1,[rch*0.25,rch*0.75],material1*1),
            "right":(-1,[rch*0.25,rch*0.75],material1*1),
        }        

        cbottom={
            #"top":(-1,[rcw/2+tw2]),
            #"bottom":(-1,[rcw/2+tw2]),
            "left":(1,[rch*0.25,rch*0.75],material1*1),
            "right":(1,[rch*0.25,rch*0.75],material1*1),
        }        


        # top
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctop,tapd=material1,tapw=taph)
        for ny1 in range(0,ny):
            for nx1 in range(0,nx):                
                q.AddRectangle(zthrough,
                               x1+cdistance2+(cdistance+cuvw)*nx1,
                               y1+cdistance1+(cdistance+cuvh)*ny1,
                               cuvw,cuvh,centered=0)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0



        # empty sides
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rchh,rch,taps=ctapsides,tapd=material1,tapw=taph)
        if 1:
            q.AddRectangle(zthrough,x1+5.0,y1+rch*0.25-taph*0.5,material1,taph,centered=0)
            q.AddRectangle(zthrough,x1+5.0,y1+rch*0.75-taph*0.5,material1,taph,centered=0)
        self.Place(V3(0,0),(p,q),0)



        #x1+=rchh+5.0
        y1+=rch+5.0


        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rchh,rch,taps=ctapsides,tapd=material1,tapw=taph)
        if 1:
            q.AddRectangle(zthrough,x1+5.0,y1+rch*0.25-taph*0.5,material1,taph,centered=0)
            q.AddRectangle(zthrough,x1+5.0,y1+rch*0.75-taph*0.5,material1,taph,centered=0)
        self.Place(V3(0,0),(p,q),0)

        x1+=rchh+5.0
        

        x1,y1=x0,y0+rch+5.0


        # bottom
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=cbottom,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)

        

    def build_jig(self,args):
        zthrough=-1.0
        material=4.0
        self.millingdistance=0.0 #0.15/2

        ################################################################################
        # standard cuvette measurements
        ################################################################################

        cuvw=12.5
        cuvh=12.5
        

        ################################################################################
        # hamamatsu cm12280 measurements
        ################################################################################
        
        x0,y0=0,0
        w,h=200.0,40.0
        w1,h1=50.0,h
        
        margin=0.1
        c12w=18.53   
        c12w2=20.12
        c12h=10.91
        c12h2=12.5
        c12d=10.12
        # from mark
        c12slitx=c12w*0.5+0.5
        c12slity=10.91*0.5
        c12d=3.2
        c12dx=0.5
        c12dy=0.0

        pcbw=30.0
        pcbh=30.0
        boxw,boxh=44.0,44.0
        # 38.5 on other part now...
        sys.stderr.write("box is %f,%f\n"%(boxw,boxh))
        c12topcbx=5.0
        c12toboxx=10.0

        # 7.5 from xleft of c12. c12 starts at -9.25 so 1.75?
        c12tolaserx=1.25
        c12tolasery=6.58
        laserd=3.60
        
        c12toledx=3.10
        c12toledy=0.0

        screwd=3.3
        screwo=3.0

        screwd=4.0
        screwo=3.5
        

        for layer in range(0,5):
            x1,y1=x0+layer*(boxw+5.0),y0
            #x1,y1=x0+0*(boxw+5.0),y0

            c12hole=1
            c12tos=0
            ledtos=0
            lasertos=0
            if layer==0:
                c12hole,c12tos,ledtos,lasertos,pcb=0,0,0,0,1
            elif layer==1:
                c12hole,c12tos,ledtos,lasertos,pcb=1,0,2,2,0
            elif layer==2:
                c12hole,c12tos,ledtos,lasertos,pcb=1,0,2,2,0
            elif layer==3:
                c12hole,c12tos,ledtos,lasertos,pcb=0,2,2,2,0
            elif layer==4:
                c12hole,c12tos,ledtos,lasertos,pcb=0,1,1,1,0

            
            toslinkin=x1+c12dx,y1+c12dy
            toslinkled=x1+c12w*0.5+c12toledx,y1+c12toledy
            toslinklaser=x1+c12tolaserx,y1-c12h*0.5-c12tolasery

            p=PolygonSet()        
            q=PolygonSet()

            if c12hole:
                q.AddRectangle(zthrough,x1-c12w*0.5-margin*0.5,y1-c12h*0.5-margin*0.5,c12w+margin,c12h+margin,r=0.0,roffset=(0,0))

            # outer border
            if pcb:
                pcbw2=pcbw+2.0
                pcbh2=pcbh+2.0
                pcbwo,pcbwq=0.0,0.0
                #q.AddRectangle(zthrough,x1-c12w*0.5-c12topcbx+1+pcbwo,y1-pcbh2*0.5,pcbw2-pcbwo,pcbh2,r=0.0,roffset=(0,0))
                pcbwo,pcbwq=0.0,5.0
                q.AddRectangle(zthrough,x1-c12w*0.5-c12topcbx+1+pcbwo,y1-pcbh2*0.5,pcbw2-pcbwo-pcbwq,pcbh2,r=0.0,roffset=(0,0))
                pcbw3,pcbh3=3,15
                q.AddRectangle(zthrough,x1-c12w*0.5-c12topcbx+1+pcbw2-1-pcbwq,y1-pcbh3/2,pcbw3+1,pcbh3,r=0.0,roffset=(0,0))
                q.Clip(0)
            
            p.AddRectangle(zthrough,x1-c12w*0.5-c12toboxx,y1-boxh*0.5,boxw,boxh,r=0.0,roffset=(0,0))

            boxx0,boxy0=x1-c12w*0.5-c12toboxx,y1-boxh*0.5
            boxx1,boxy1=boxx0+boxw,boxy0+boxh
            q.AddCircle(zthrough,boxx0+screwo,boxy0+screwo,screwd,nverts=64)
            q.AddCircle(zthrough,boxx1-screwo,boxy0+screwo,screwd,nverts=64)
            q.AddCircle(zthrough,boxx0+screwo,boxy1-screwo,screwd,nverts=64)
            q.AddCircle(zthrough,boxx1-screwo,boxy1-screwo,screwd,nverts=64)
            

            #p.Clip(0)
            self.Place(V3(0,0),(p,q),0)

            # toslink in
            q=self.GetToslinkConnector(zthrough,level=c12tos,pos=toslinkin)        
            self.Place(V3(0,0),(None,q),0)

            # toslink led
            q=self.GetToslinkConnector(zthrough,level=ledtos)
            self.Place(V3(*toslinkled),(None,q),90)

            # toslink laser
            q=self.GetToslinkConnector(zthrough,level=lasertos)
            #q.AddRectangle(zthrough,0,0,12.0,12.0,centered=1)
            self.Place(V3(*toslinklaser),(None,q),0)



        ################################################################################
        # cuvette
        ################################################################################

        x1,y1=x0+(layer+1)*(boxw+5.0),y0
        x1,y1=x0,y0+boxh+5.0
        

        material1=4.0
        cmargin=1.0
        taph=8.0
        tapw=taph
        d=material1
        rcw=cuvw+d*4+0.5*2+cmargin+cmargin
        #rcw=44.0

        rch=cuvw+d*2+cmargin+cmargin

        x1,y1=x0-rcw,y0+rch+5

        #rcw=80.0
        #rch=40.0

        tw2=0 #rcw/2-tapw/2 #-material1
        th2=0 #rch/2-taph/2-material1/2 #+material1
        ctapsides={"top":(1,[rcw/2+tw2]),
                "bottom":(1,[rcw/2+tw2]),
                "left":(-1,[rch/2+th2],material1*2),
                "right":(-1,[rch/2+th2],material1*2),
        }        

        ctopbottom={"top":(-1,[rcw/2+tw2]),
                    "bottom":(-1,[rcw/2+tw2]),
                    "left":(-1,[rcw/2+tw2],material1*1),
                    "right":(-1,[rcw/2+tw2],material1*1),
        }        
        ctaps4={
            "top":(1,[rcw/2+tw2]),
            "bottom":(1,[rcw/2+tw2]),
            "left":(1,[rch/2+th2]),
            "right":(1,[rch/2+th2]),
        }        
        th2=-material1 #*0.5
        ctaps41={
            #"top":(1,[rcw/2+tw2]),
            #"bottom":(1,[rcw/2+tw2]),
            "left":(1,[rch/2+th2]),
            "right":(1,[rch/2+th2]),
        }        
        th2=0

        # height is 38.500000
        # height is 22.500000, half is 11.25
        sys.stderr.write("height is %f\n"%rch)
        sys.stderr.write("rcw is %f\n"%rcw)

        #height is 22.500000
        #rcw is 38.500000


        # empty sides
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctapsides,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0


        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctapsides,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0


        # side 1 with toslink
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctaps4,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
        q=self.GetToslinkConnector(zthrough,level=1,pos=(x1+rcw/2,y1+rch/2))
        self.Place(V3(0,0),(None,q),0)
        x1+=rcw+5.0

        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1+material1,rcw,rch-material1*2,taps=ctaps41,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
        q=self.GetToslinkConnector(zthrough,level=2,pos=(x1+rcw/2,y1+rch/2))
        self.Place(V3(0,0),(None,q),0)
        x1+=rcw+5.0



        # side 2 with toslink
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctaps4,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
        q=self.GetToslinkConnector(zthrough,level=1,pos=(x1+rcw/2,y1+rch/2))
        self.Place(V3(0,0),(None,q),0)
        x1+=rcw+5.0

        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1+material1,rcw,rch-material*2,taps=ctaps41,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
        q=self.GetToslinkConnector(zthrough,level=2,pos=(x1+rcw/2,y1+rch/2))
        self.Place(V3(0,0),(None,q),0)
        x1+=rcw+5.0



        # top
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rcw,taps=ctopbottom,tapd=material1,tapw=taph)
        q.AddRectangle(zthrough,x1+rcw/2,y1+rcw/2,cuvw,cuvh,centered=1)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0

        # bottom
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rcw,taps=ctopbottom,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
            
        x1+=rcw+5.0

        # lid
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rcw,taps=ctopbottom,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)



        # bug - without tap at bottom ....
        th2=material1*0.5
        ctapsides2={"top":(1,[rcw/2+tw2]),
                "left":(-1,[rch/2+th2]),
                "right":(-1,[rch/2+th2]),
        }        
        ctaps42={"top":(1,[rcw/2+tw2]),
                "left":(1,[rch/2+th2]),
                "right":(1,[rch/2+th2]),
        }        

        x1+=rcw+5.0

        # empty sides
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctapsides2,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0

        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctapsides2,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)

        x1+=rcw+5.0

        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctaps42,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
        x1+=rcw+5.0

        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,rcw,rch,taps=ctaps42,tapd=material1,tapw=taph)
        self.Place(V3(0,0),(p,q),0)
        x1+=rcw+5.0

            


    def GetToslinkConnector(self,zthrough,level,pos=None):
        #proporitions 2.1 vs 1.5 and 1.14 wide on top, 2.0 tall
        # + 0.2 is exact size except for taps on top and bottom not fitting
        # + 0.2 + 0.2 is loose and wiggly 
        # probably we should just add slightly vertically
        toslinkw=6.0+0.2
        toslinkh=6.0+0.2
        toslinkhtap=toslinkh+0.2
        toslinkwtap=2.6
        toslinkh2=toslinkh*3/4
        toslinkgd=1.0+0.2
        toslinkpind=2.6+0.1
        if pos==None:
            x1,y1=0.0,0.0
        else:
            x1,y1=pos

        q=PolygonSet()

        if level==1:
            q.AddRoundedRectangle(zthrough,x1,y1,toslinkw,toslinkh,radius=toslinkh*1/4,nverts=1,centered=1)
            q.AddRectangle(zthrough,x1,y1-(toslinkh-toslinkh2)/2,toslinkw,toslinkh2,centered=1)
            q.AddCircle(zthrough,x1-toslinkw/2,y1,toslinkgd,nverts=64)
            q.AddCircle(zthrough,x1+toslinkw/2,y1,toslinkgd,nverts=64)
            q.AddRectangle(zthrough,x1,y1,toslinkwtap,toslinkhtap,centered=1)

        elif level==2:
            q.AddCircle(zthrough,x1,y1,toslinkpind,nverts=64)
        else:
            pass


        q.Union()
        return q





















    def build_disabled(self,args):
        """
        nanodrop variant with toslink
        2x sides
        1x camera holder
        1x top
        1x bottom
        1x front with toslink and double layers with slit
        1x back with exit for cable.
        """
        sideh=110.096
        sidew=69.094
        sidex=8.501
        sidey=152.202
        sidecammountcenterx=16.689+10.300/2
        sidecammountcentery=241.547+7.196/2
        sidecammountangle=26.0
        sidecammountcenterx=40.035+10.399/2
        sidecammountcentery=230.100+7.197/2
        sidecammountw=10.102
        sidecammounth=3.106
        topholderfrontx=50.499
        topholderfronty=166.203
        topholderfrontw=6.100
        topholderfronth=10.101
        topholderbackx=53.498
        topholderbacky=244.197
        topholderbackw=3.100
        topholderbackh=10.099
        frontholderbottomx=16.506
        frontholderbottomy=154.197
        frontholderbottomw=10.099
        frontholderbottomh=3.100


                
        

        # how to take off 45 degree angle on all blocks?
        

        zthrough=-1.0
        material=4.0
        w,h,d=110.0,50.0,45.0

        x0,y0=0.0,0.0

        # left
        x1,y1=x0,y0
        # distance to slit plate
        slitdistance=20.0
        taph=10.0
        taph1=taph+0.1
        material1=material+0.1

        tapp=20
        h2=h/2-material
        ctaps={"top":(-1,[tapp,-tapp]),
               "bottom":(-1,[tapp,-tapp]),
               "left":(-1,[h/2]),
               "right":(-1,[h/2],(material1)*2),
        }        

        # height of toslink connector
        arcd=30.0
        oy=0.0
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,w,h,taps=ctaps,tapd=material1,tapw=taph1)
        q.AddRectangle(zthrough,x1+w-((material+0.1)*2+slitdistance),y1+h/2-(taph+0.5)/2,material1,taph1)

        if 0:
            q.AddThickArc(zthrough,x1+h/2,y1+h/2,arcd,-45,-90,3.1,nverts=128)
            q.AddThickArc(zthrough,x1+h/2,y1+h/2,arcd,-45+180,-90,3.1,nverts=128)
        elif 0:
            q2=PolygonSet()
            q2.AddRectangle(zthrough,0.0,-13.0,material,10.0,centered=1)
            q2.AddRectangle(zthrough,0.0,+13.0,material,10.0,centered=1)
            self.Place(V3(x1+h/2,y1+h/2),(None,q2),26.0)
        else:
            q.AddRoundedRectangle(zthrough,x1+20.0+25.0,y1+h/2,3.0,h-20.0,1.5,nverts=16)
            q.AddRoundedRectangle(zthrough,x1+20.0,y1+h/2,3.0,h-20.0,1.5,nverts=16)
        self.Place(V3(0,0),(p,q),0)

        # right
        y1+=h+5.0
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,w,h,taps=ctaps,tapd=material1,tapw=taph1)
        q.AddRectangle(zthrough,x1+w-((material+0.1)*2+slitdistance),y1+h/2-(taph+0.5)/2,material1,taph1)

        if 0:
            q.AddThickArc(zthrough,x1+h/2,y1+h/2,arcd,-45,-90,3.1,nverts=128)
            q.AddThickArc(zthrough,x1+h/2,y1+h/2,arcd,-45+180,-90,3.1,nverts=128)
        elif 0:
            q2=PolygonSet()
            q2.AddRectangle(zthrough,0.0,-13.0,material,10.0,centered=1)
            q2.AddRectangle(zthrough,0.0,+13.0,material,10.0,centered=1)
            self.Place(V3(x1+h/2,y1+h/2),(None,q2),-26.0)
        else:
            q.AddRoundedRectangle(zthrough,x1+20.0+25.0,y1+h/2,3.0,h-20.0,1.5,nverts=16)
            q.AddRoundedRectangle(zthrough,x1+20.0,y1+h/2,3.0,h-20.0,1.5,nverts=16)
        self.Place(V3(0,0),(p,q),0)

        y1+=h+5.0

        w2=w-material*2
        h2=h-material*2

        ctaps3={"top":(1,[tapp,-tapp]),
                "bottom":(1,[tapp,-tapp]),
                "left":(-1,[d/2]),
                "right":(-1,[d/2],(material1)*2),
        }        
        ctaps2={"top":(1,[d/2]),
                "bottom":(1,[d/2]),
               "left":(1,[h/2]),
               "right":(1,[h/2]),
        }        
        # top
        p,q=PolygonSet(),PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,w,d,taps=ctaps3,tapd=material1,tapw=taph)
        q.AddRectangle(zthrough,x1+w-((material+0.1)*2+slitdistance),y1+d/2-(taph+0.5)/2,material+0.1,taph+0.1)
        self.Place(V3(0,0),(p,q),0)

        # bottom
        y1+=h+5.0
        p,q=PolygonSet(),PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,w,d,taps=ctaps3,tapd=material1,tapw=taph)
        if 0:
            # cut off 45 degree angle!
            p.AddRectangleWithTaps2(zthrough,x1-160.0,y1-50.0,d,h,taps=ctaps2,tapd=material1,tapw=taph,r=math.pi/4)
            p.Clip(2)
        q.AddRectangle(zthrough,x1+w-((material+0.1)*2+slitdistance),y1+d/2-(taph+0.5)/2,material+0.1,taph+0.1)
        self.Place(V3(0,0),(p,q),0)

        
        # front - toslink jack
        x0+=w+5.0
        x1,y1=x0,y0
        d2=d-material*2
        h2=h-material*2
        p=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,d,h,taps=ctaps2,tapd=material1,tapw=taph)

        q=PolygonSet()
        #proporitions 2.1 vs 1.5 and 1.14 wide on top, 2.0 tall
        # + 0.2 is exact size except for taps on top and bottom not fitting
        # + 0.2 + 0.2 is loose and wiggly 
        # probably we should just add slightly vertically
        toslinkw=6.0+0.2
        toslinkh=6.0+0.2
        toslinkhtap=toslinkh+0.2
        toslinkwtap=2.6
        toslinkh2=toslinkh*3/4
        toslinkgd=1.0+0.2
        toslinkpind=2.6+0.1
        q.AddRoundedRectangle(zthrough,x1+d/2,y1+h/2+oy,toslinkw,toslinkh,radius=toslinkh*1/4,nverts=1,centered=1)
        q.AddRectangle(zthrough,x1+d/2,y1+h/2+oy-(toslinkh-toslinkh2)/2,toslinkw,toslinkh2,centered=1)
        q.AddCircle(zthrough,x1+d/2-toslinkw/2,y1+h/2+oy,toslinkgd,nverts=64)
        q.AddCircle(zthrough,x1+d/2+toslinkw/2,y1+h/2+oy,toslinkgd,nverts=64)
        q.AddRectangle(zthrough,x1+d/2,y1+h/2+oy,toslinkwtap,toslinkhtap,centered=1)
        
        q.Union()
        self.Place(V3(0,0),(p,q),0)
        self.Place(V3(d+2.0,0),(p,q),0)

        y1+=h+5.0
        
        # front - toslink center
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,d,h,taps=ctaps2,tapd=material1,tapw=taph)
        q.AddCircle(zthrough,x1+d/2,y1+h/2+oy,toslinkpind,nverts=64)
        self.Place(V3(0,0),(p,q),0)
        self.Place(V3(d+2.0,0),(p,q),0)

        y1+=h+5.0
        
        # front - 3.5 mm
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,d,h,taps=ctaps2,tapd=material1,tapw=taph)
        q.AddCircle(zthrough,x1+d/2,y1+h/2+oy,3.7,nverts=64)
        self.Place(V3(0,0),(p,q),0)
        self.Place(V3(d+2.0,0),(p,q),0)

        y1+=h+5.0
        
        # front - slit
        slitw,slith=5.1,0.1
        p=PolygonSet()
        q=PolygonSet()
        p.AddRectangleWithTaps2(zthrough,x1,y1,d,h,taps=ctaps2,tapd=material1,tapw=taph)
        q.AddRectangle(zthrough,x1+d/2,y1+h/2+oy,slitw,slith,centered=1)
        self.Place(V3(0,0),(p,q),0)


        y1+=h+5.0
        
        # back - cable exit
        p=PolygonSet()
        cabled=5.0
        p.AddCircle(zthrough,x1+d/4,y1+cabled/2+material,cabled,nverts=64)
        p.AddRectangle(zthrough,x1+d/4-cabled/2,y1+cabled/2-cabled+material,cabled,cabled)
        p.Union()
        p.AddRectangleWithTaps2(zthrough,x1,y1,d,h,taps=ctaps2,tapd=material1,tapw=taph)
        p.Clip(operation=1)
        
        self.Place(V3(0,0),(p,None),0)

        x1+=d+2.0
        y1-=h+5.0

        # camera holder
        cholew=25.0
        choleh=30.0
        cscrewd=24.3 
        ctapw=2.6
        ctaph=4.9
        cscrewd2=cscrewd/2
        p=PolygonSet()
        slack=6.0
        hscrewd=2.6
        p.AddRectangle(zthrough,x1+material,y1+material+slack/2,d2,h2-slack)
        p.AddRectangle(zthrough,x1+material,y1+material+h2/2,8.0,hscrewd,centered=1)
        p.AddRectangle(zthrough,x1+material+d2,y1+material+h2/2,8.0,hscrewd,centered=1)
        p.Clip(operation=2)

        q=PolygonSet()
        q.AddRectangle(zthrough,x1+material+d2/2,y1+material+h2/2,cholew-ctapw*2,choleh,        centered=1)
        q.AddRectangle(zthrough,x1+material+d2/2,y1+material+h2/2,cholew        ,choleh-ctaph*2,centered=1)
        q.Union()
        q.AddCircle(zthrough,x1+material+d2/2-cscrewd2,y1+material+h2/2-cscrewd2,1.5,nverts=32)
        q.AddCircle(zthrough,x1+material+d2/2+cscrewd2,y1+material+h2/2-cscrewd2,1.5,nverts=32)
        q.AddCircle(zthrough,x1+material+d2/2-cscrewd2,y1+material+h2/2+cscrewd2,1.5,nverts=32)
        q.AddCircle(zthrough,x1+material+d2/2+cscrewd2,y1+material+h2/2+cscrewd2,1.5,nverts=32)
        self.Place(V3(0,0),(p,q),0)


        # add more fronts: toslink1, toslink2 and slit holder
        # cable exit in back
        # camera holder
        # grating holder in bottom
        




def main(argv,stdout,environ):
  progname = argv[0]
  args=argv[1:]

  gcode_mill_cut=None
  depth=0.0
  ox,oy=0.0,0.0
  feed_speed=None
  spindle_speed=None
  encoder_scale=1.0    # for getting scale right in inkscape and laser cutter at fablab berlin etc
  encoder_scale=0.8   # for getting scale right in illustrator on laser cutter at te
  #encoder_scale=1.00624*0.91666 # for getting scale right in coreldraw
  i=0
  show=0
  safe=0.0
  action=[]
  while i<len(args):
      if args[i].startswith("-"):          
          if args[i]=="-mill":
              i+=1
              gcode_mill_cut=args[i]
          elif args[i]=="-scale":
              i+=1
              encoder_scale=float(args[i])
          elif args[i]=="-feed":
              i+=1
              feed_speed=float(args[i])
          elif args[i]=="-spindle":
              i+=1
              spindle_speed=float(args[i])
          elif args[i]=="-x":
              i+=1
              ox=float(args[i])
          elif args[i]=="-depth":
              i+=1
              depth=float(args[i])
          elif args[i]=="-safe":
              safe=40.0
          elif args[i]=="-y":
              i+=1
              oy=float(args[i])
          elif args[i]=="-show":
              show=1
      else:
          action+=[args[i]]
      i+=1


  miller=Miller((ox,oy))
  miller.action=" ".join(action)
  miller.gcode_mill_cut=gcode_mill_cut

  miller.zborderstep=None #0.5 #None #0.5 #1.8 #None #1.8 #1.0 #None #1.0 #None #5.2/3+0.1
  miller.zabove=5.0
  miller.zsafe=safe
  miller.zmillthrough=-5.5
  if feed_speed: miller.feed_speed=feed_speed
  if spindle_speed: miller.spindle_speed=spindle_speed
  
  print miller.Run(args)

  print "Consolidating"
  t0=time.time()
  miller.Consolidate()


  print "   took",time.time()-t0,"seconds"
  print "Finishing"
  t0=time.time()
  miller.FinishPaths(1)
  print "   took",time.time()-t0,"seconds"
  #nc=NCEncoder()
  nc=SVGEncoder(scale=encoder_scale,fill=0) 
  nc.Begin()
  print "Encoding"
  t0=time.time()
  d,e=miller.EncodeCut(nc)
  nc.End()
  print "   took",time.time()-t0,"seconds"
  print "traces will take ca %d:%02d to mill, milling %f mm"%(d/60,d%60,e)
  if gcode_mill_cut:
      file(gcode_mill_cut,"w").write(str(nc))

  if show:
      def millingupdater(caller):
          caller.millingdistance=miller.millingdistance
          miller.FinishPaths()
          caller.millingpath=miller.millingpath
          caller.polygon_set=miller.polygon_set
          caller.inverted_set=miller.inverted_set
          print "milling updater says milling distance is",miller.millingdistance
      app=Viewer.TestApplication(800,600,0,args)
      scene=Viewer.TestScene("test",app,None)
      scene.toolsize=miller.millingdistance*2.0
      scene.millingupdater=millingupdater
      scene.polygons=miller.polygons
      scene.inverted=miller.inverted
      scene.ncpath=nc.path
      app.camdist,app.altitude,app.ry=413.352059982,595.552089972,1.88495559215

      app.Scenes.Add(scene)
      app.Load()
      app.Scenes.Set("test")
      app.Run()
      



if __name__ == "__main__":
  main(sys.argv, sys.stdout, os.environ)
        

import os,sys,string,time,math,random
import sexpdata 
from numpy import array
#import pykicad.pcb





def main(argv,stdout,environ):
  progname = argv[0]
  args=argv[1:]

  pcbpath=args[0]

  pcb=sexpdata.load(file(pcbpath))

  #pykicad.pcb.Pcb.from_file(pcbpath)

  positions={}

  edgecut=[]
  
  for k in pcb:
      if type(k)==list:
          name=sexpdata.dumps(k[0])
          if name=="gr_line":
              start=None
              end=None
              angle=None
              layer=None
              width=None
              for sym in k[1:]:
                  if type(sym)==list:
                      symname=sexpdata.dumps(sym[0])
                      #print symname,sexpdata.dumps(sym[1])
                      if symname=="start":
                          start=float(sym[1]),float(sym[2])
                      elif symname=="end":
                          end=float(sym[1]),float(sym[2])
                      elif symname=="angle":
                          angle=float(sym[1])
                      elif symname=="width":
                          width=float(sym[1])
                      elif symname=="layer":
                          layer=sexpdata.dumps(sym[1])
              if layer=="Edge\\.Cuts":
                  print "line",layer,start,end,angle,width
                  edgecut+=[["l",start,end,angle,width]]
          elif name=="gr_arc":
              for sym in k[1:]:
                  if type(sym)==list:
                      symname=sexpdata.dumps(sym[0])
                      if symname=="start":
                          start=float(sym[1]),float(sym[2])
                      elif symname=="end":
                          end=float(sym[1]),float(sym[2])
                      elif symname=="angle":
                          angle=float(sym[1])
                      elif symname=="width":
                          width=float(sym[1])
                      elif symname=="layer":
                          layer=sexpdata.dumps(sym[1])
              if layer=="Edge\\.Cuts":
                  print "arc",layer,start,end,angle,width
                  edgecut+=[["a",start,end,angle,width]]
          elif name=="module":
              what=sexpdata.dumps(k[1])
              """
              (u'module', u'GaudiLabsFootPrints:MountingHolePad_3x9mm', 
               [[Symbol('layer'), Symbol('F.Cu')], 
                [Symbol('tedit'), Symbol('5ACA64CE')], 
                [Symbol('tstamp'), Symbol('57B36F09')], 
                [Symbol('at'), 56, 74], 
                 [Symbol('descr'), 'Mounting hole, Befestigungsbohrung, 3mm, No Annular, Kein Restring,'], [Symbol('tags'), 'Mounting hole, Befestigungsbohrung, 3mm, No Annular, Kein Restring,'], [Symbol('path'), Symbol('/5737C5A3')], [Symbol('fp_text'), Symbol('reference'), Symbol('P13'), [Symbol('at'), 8.194, -7.171], [Symbol('layer'), Symbol('F.SilkS')], Symbol('hide'), [Symbol('effects'), [Symbol('font'), [Symbol('size'), 1, 1], [Symbol('thickness'), 0.15]]]]])

              """
              ref=None
              pos=None
              for sym in k:
                  if type(sym)==list:
                      symname=sexpdata.dumps(sym[0])
                      #print symname
                      #     (fp_text reference P13 (at 8.194 -7.171) (layer F.SilkS) hide

                      if symname=="fp_text":
                          fpt=sexpdata.dumps(sym[1])
                          if fpt=="reference":
                              ref=sexpdata.dumps(sym[2])
                      elif symname=="at":
                          pos=sym[1:3]
                      
              if ref!=None and pos!=None:
                  positions[ref]=pos
              
              #print (name,what,ref,pos)

  refkeys=positions.keys()
  refkeys.sort()
  
  for ref in refkeys:
      pos=positions[ref]
      #print "%-20s"%ref,pos


  print "positions="+repr(positions)
  print "edgecut="+repr(edgecut)

  p13=positions["P13"]
  p14=positions["P14"]
  p15=positions["P15"]
  p16=positions["P16"]

  print p13,p14,p15,p16

  f1=positions["FLUXL_8_1"]
  f2=positions["FLUXL_9_1"]
  
  f3=positions["FLUXL_8_8"]
  f4=positions["FLUXL_9_8"]

  print 

  print f1,f2,f3,f4


  for a in edgecut:
      print a

  # gr_arc
  # gr_line

  # (gr_line (start 150 35) (end 150 136) (angle 90) (layer Edge.Cuts) (width 0.1))


  #print pcb.dump()



if __name__ == "__main__":
  main(sys.argv, sys.stdout, os.environ)
